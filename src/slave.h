/**********************************************************************
 *   FST BMS --- slave
 *
 *   Main slave module header
 *      - global parameters
 *      - global variables
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __SLAVE_H__
#define __SLAVE_H__

#define STRING(s) #s
#define XSTRING(s) STRING(s)
/*#pragma message ("blablabla define XXX=" XSTRING(XXX))*/

#include "Common/config.h"

/*
 * Global Variables; remember to initialize (!)
 */

#ifdef OWNER
	#define EXTERN
#else
	#define EXTERN extern	/* declares external variables */
#endif

/* careful with race conditions! */

EXTERN volatile unsigned long long _time;		/* time in ms since turn on (64bits); required for integration of current at least! */

EXTERN volatile unsigned int _cell_SOC[12];	/* %*10 */
EXTERN volatile unsigned int _SOC;			/* %*10 */
EXTERN volatile unsigned int _highest_SOC;	/* %*10 */
EXTERN volatile unsigned int _lowest_SOC;	/* %*10 */

EXTERN volatile int _temperatures[TEMP_SENSORS_N+4];		/* holds up-to-date temperatures values in ºC*10; Cells, MPU, LTC and Balancing circuit(x2) */
EXTERN volatile int _highest_cell_T;
EXTERN volatile int _lowest_cell_T;
EXTERN volatile int _average_cell_T;
EXTERN volatile int _highest_slave_T;	/* between MPU and LTC */
EXTERN volatile int _lowest_slave_T;	/* between MPU and LTC */
EXTERN volatile int _highest_load_T;

EXTERN volatile unsigned int _voltages[12];	/* holds up-to-date voltages values in mV*10 */
EXTERN volatile unsigned long _total_V;
EXTERN volatile unsigned int _average_V;
EXTERN volatile unsigned int _highest_V;
EXTERN volatile unsigned int _lowest_V;

/* holds the threshold for balancing in mV*10; should start as V_CRITICAL_H and lowered by the master module */
EXTERN volatile unsigned int _balancing_target;

/* 16bit mask for the 12 cells status: should read 0b0000ssssssssssss where s=0 *
 * for NOT balancing and s=1 for balancing; LSB is cell 1, MSB is cell 12       */
EXTERN volatile unsigned int _balance_mask;

/* 16bit mask for the health of the (up to) 12 NTCs connections: 0bss?sssssssssssss *
 * where s=1 for bad connection and 0 otherwise; ? is for the LTC temperature which *
 * isn't a NTC, value doesn't mean anything and may be 0 or 1;                      *
 * balancing load 2, balancing load 1, ?, MPU, cell temp 12, ..., cell temp 1       */
EXTERN volatile unsigned int _temp_fault_mask;

/* 16bit mask for the health of the 12 cell pole connections: 0b000sssssssssssss *
 * where s=1 for bad connection and 0 otherwise; LSB is C0, MSB is C12.          *
 * Note that for a supply fault is impossible to detect bad wiring directly: the *
 * slave should detect that it can't communicate with the LTC and report that    *
 * instead.                                                                      */
EXTERN volatile unsigned int _volt_fault_mask;


EXTERN volatile unsigned int _monitor_period;	/* monitoring period in ms */
EXTERN volatile unsigned int _op_mode;			/* mode of operation */


/*
 * Prototypes
 */

int set_CPU_priority(unsigned int n);
int set_mode(unsigned int mode);
void main_loop(void);

#define RESET() {\
	__asm__ volatile("reset");\
}


#endif
