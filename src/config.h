/**********************************************************************
 *   FST BMS --- slave
 *
 *   Code revision stamp
 *   Slave ID
 *   Configuration parameters
 *   Error codes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "CAN_IDs/CAN_ID.h"


/*
 * Firmware revision: FIRMWARE_H.FIRMWARE_L
 */

#define FIRMWARE_H 0
#define FIRMWARE_L 5


/*
 * Configuration parameters
 */

#define CAN_ID	CAN_ID_BMS_SLAVE(0)


/*
 * Monitoring levels
 * corresponds to monitoring periods in ms:
 */

#define MT_NORMAL		1000
#define MT_ALERT		1000
#define MT_CRITICAL		1000


/*
 * Internal definitions
 */

/* CPU core clock and instruction clock values
   ADC should be tuned if this is changed      */
#define FOSC 30000000UL
#define FCY FOSC/4


#endif
