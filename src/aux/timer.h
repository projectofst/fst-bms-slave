/**********************************************************************
 *   FST BMS --- slave
 *
 *   Timer
 *      - parameters
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __TIMER_H__
#define __TIMER_H__


/*
 * Parameters
 */

/* Timer value at Fcy that corresponds to 1ms for 1:1 scale */
#define M_SEC_1_1 FCY*0.001

/* Timer value at Fcy that corresponds to 1ms for 1:256 scale; NOT VERY ACCURATE! */
#define M_SEC_1_256 FCY/256*0.001



/*
 * Prototypes
 */

void timer1_config(void);
void timer2_config(void);

void update_loop_period(unsigned int period);


#endif
