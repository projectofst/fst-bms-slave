/**********************************************************************
 *   FST BMS --- slave
 *
 *   Delay macros
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __DELAY_H__
#define __DELAY_H__

#include "config.h"

extern void __delay32(unsigned long cycles);

/* FCY must(!) be defined */

/**********************************************************************
 * Name:	__delay_ms
 * Args:	unsigned long long
 * Return:	-
 * Desc:	Delays for x (ms)
 **********************************************************************/
#define __delay_ms(x) __delay32( (unsigned long) (((unsigned long long) x)*(FCY)/1000ULL) );

 /**********************************************************************
 * Name:	__delay_us
 * Args:	unsigned long long
 * Return:	-
 * Desc:	Delays for x (us)
 **********************************************************************/
#define __delay_us(x) __delay32( (unsigned long) (((unsigned long long) x)*(FCY)/1000000ULL) );


#endif
