/**********************************************************************
 *   FST BMS --- slave
 *
 *   CAN
 *      - device configuration
 *      - interruption assignment
 *      - top level communication functions
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "Common/CANcodes.h"
#include "config.h"
#include "slave.h"
#include "CAN.h"
#include "voltages.h"
#include "temperatures.h"
#include "balancing.h"
#include "delay.h"


void answer_SOC(CANdata *msg);
void answer_top_and_bottom_SOC(CANdata *msg);
void answer_top_and_bottom_voltages(CANdata *msg);
void answer_average_voltage(CANdata *msg);
void answer_voltages(unsigned int code, CANdata *msg);
void answer_cell_top_and_bottom_temperatures(CANdata *msg);
void answer_cell_average_temperatures(CANdata *msg);
void answer_cell_temperatures(unsigned int code, CANdata *msg);
void answer_slave_temperatures(CANdata *msg);
void answer_fault_masks(CANdata *msg);
void answer_balancing(CANdata *msg);


/**********************************************************************
 * Name:	CAN1_config
 * Args:	-
 * Return:	-
 * Desc:	Configures CAN 1 channel.
 **********************************************************************/
void CAN1_config(void){

	/* assuming FOSC 30MHz */

	TRISFbits.TRISF0 = 1;				/* CANRX input */
	TRISFbits.TRISF1 = 0;				/* CANTX output */

	C1CTRLbits.REQOP = 4;			/* request configuration mode */
	while(C1CTRLbits.OPMODE!=4);	/* wait for mode to be set */

	/* control */
	C1CTRLbits.CANCAP = 0;			/* disable CAN capture */
	C1CTRLbits.CSIDL = 0;			/* continue on Idle mode */
	C1CTRLbits.CANCKS = 0;			/* master clock 4*FCY */

	/* baudrate */
	C1CFG1bits.BRP = 0;				/* TQ = 2/FCAN */
	C1CFG2bits.SEG2PHTS = 1;		/* Freely programmable */
	C1CFG2bits.PRSEG = 1;			/* propagation time segment bits length 2 x TQ */
	C1CFG1bits.SJW = 0;				/* re-synchronization jump width time is 1 x TQ */
	C1CFG2bits.SEG1PH = 7;			/* phase 1 segment length 8 x TQ */
	C1CFG2bits.SEG2PH = 4;			/* phase 2 segment length 5 x TQ <= phase1+prop.seg. && > resync jump */

	/* receive buffers */
	C1RX0CONbits.DBEN = 1;			/* RX0 full will write to RX1 */
	C1RXF0SID = 8;					/* probably both wrong (filter set but not used) and unnecessary ???!!!! */
	C1RXF1SID = 8;					/* " */
	C1RXM0SID = 0;					/* match all messages */
	C1RXM1SID = 0;					/* " */

	/* interrupts */
	C1INTEbits.RX0IE = 1;			/* RX0 interrupt enabled */
	C1INTEbits.RX1IE = 1;			/* RX1 interrupt enabled */
	IFS1bits.C1IF = 0;				/* Clear CAN1 Flag */
	IPC6bits.C1IP = 5;				/* CAN1 priority is 5 */
	IEC1bits.C1IE = 1;				/* CAN1 interrupts enabled */

	C1TX0CONbits.TXPRI = 3;
	C1RX0CONbits.RXFUL = 0;
	C1RX1CONbits.RXFUL = 0;

	C1CTRLbits.REQOP = 0;			/* request normal mode */
	while(C1CTRLbits.OPMODE!=0);	/* wait for normal mode to be set */

	return;
}


/**********************************************************************
 * Name:	CAN1_send
 * Args:	CANdata *CANmessage
 * Return:	exit status
 * Desc:	Sends CAN message.
 **********************************************************************/
int CAN1_send(CANdata *CANmessage){

	static int try_n=0;
	unsigned int int_sid=0;

	int_sid = CANmessage->sid<<5;
	int_sid = int_sid & (0b1111100000000000);
	int_sid = int_sid | ((CANmessage->sid<<2)&(0b0000000011111100));

	if(!C1TX0CONbits.TXREQ){	/* occupied? */
		C1TX0SID = int_sid;
		C1TX0SIDbits.TXIDE = 0;
		C1TX0SIDbits.SRR = 0;
		C1TX0DLC = 0;
		C1TX0DLCbits.DLC = CANmessage->dlc;
		C1TX0B1 = CANmessage->data[0];
		C1TX0B2 = CANmessage->data[1];
		C1TX0B3 = CANmessage->data[2];
		C1TX0B4 = CANmessage->data[3];
		C1TX0CONbits.TXREQ = 1;
	}else if(!C1TX1CONbits.TXREQ){
		C1TX1SID = int_sid;
		C1TX1SIDbits.TXIDE = 0;
		C1TX1SIDbits.SRR = 0;
		C1TX1DLC = 0;
		C1TX1DLCbits.DLC = CANmessage->dlc;
		C1TX1B1 = CANmessage->data[0];
		C1TX1B2 = CANmessage->data[1];
		C1TX1B3 = CANmessage->data[2];
		C1TX1B4 = CANmessage->data[3];
		C1TX1CONbits.TXREQ = 1;
	}else if(!C1TX2CONbits.TXREQ){
		C1TX2SID = int_sid;
		C1TX2SIDbits.TXIDE = 0;
		C1TX2SIDbits.SRR = 0;
		C1TX2DLC = 0;
		C1TX2DLCbits.DLC = CANmessage->dlc;
		C1TX2B1 = CANmessage->data[0];
		C1TX2B2 = CANmessage->data[1];
		C1TX2B3 = CANmessage->data[2];
		C1TX2B4 = CANmessage->data[3];
		C1TX2CONbits.TXREQ = 1;

	}else if(try_n > 2){
		/* too many failures, clear buffers and return error */
		try_n = 0;
		C1TX0CONbits.TXREQ = 0;
		C1TX1CONbits.TXREQ = 0;
		C1TX2CONbits.TXREQ = 0;
		return -2;
	}else{
		try_n++;
		return -1;
	}
	/* reset number of tries */
	try_n = 0;
	return 0;
}


/**********************************************************************
 * Name:	CAN_handler
 * Args:	CANdata msg
 * Return:	exit status
 * Desc:	Parses received message for correctness and executes
 *          appropriate functions.
 **********************************************************************/
int CAN_handler(CANdata msg){

	CANdata reply;

	/*
	 * TODO: mask certain functions with warning messages if requested
	 * data is not available or out of date because of errors within
	 * the module.
	 */


	/* http://www.youtube.com/watch?v=QBN5eHAFDtg */
	if((msg.sid == CAN_ID_BMS_QUERY) && (msg.data[2] == CAN_ID)){

		/*
		 * Queries
		 */

		switch(msg.dlc){

			case 6:
				switch(msg.data[1]){

					case CAN_Q_PING:
						msg.sid = CAN_ID;
						msg.dlc = 4;
						msg.data[0] = _time;
						while(CAN1_send(&msg)==-1);
						break;

					case CAN_Q_SOC:
						answer_SOC(&reply);
						while(CAN1_send(&reply)==-1);
						break;

					case CAN_Q_TB_SOC:
						answer_top_and_bottom_SOC(&reply);
						while(CAN1_send(&reply)==-1);
						break;

					/*case CAN_Q_SOH:
						break;*/

					case CAN_Q_TB_VOLT:
						answer_top_and_bottom_voltages(&reply);
						while(CAN1_send(&reply)==-1);
						break;
					case CAN_Q_AVG_VOLT:
						answer_average_voltage(&reply);
						while(CAN1_send(&reply)==-1);
						break;
					case CAN_Q_TB_CTEMP:
						answer_cell_top_and_bottom_temperatures(&reply);
						while(CAN1_send(&reply)==-1);
						break;
					case CAN_Q_AVG_CTEMP:
						answer_cell_average_temperatures(&reply);
						while(CAN1_send(&reply)==-1);
						break;
					case CAN_Q_STEMP:
						answer_slave_temperatures(&reply);
						while(CAN1_send(&reply)==-1);
						break;

					case CAN_Q_BALANCING:
						answer_balancing(&reply);
						while(CAN1_send(&reply)==-1);
						break;
					case CAN_Q_FMASKS:
						answer_fault_masks(&reply);
						while(CAN1_send(&reply)==-1);
						break;

					case CAN_Q_VOLTAGES_0:
					case CAN_Q_VOLTAGES_1:
					case CAN_Q_VOLTAGES_2:
					case CAN_Q_VOLTAGES_3:
					case CAN_Q_VOLTAGES_4:
					case CAN_Q_VOLTAGES_5:
						answer_voltages(msg.data[1], &reply);
						while(CAN1_send(&reply)==-1);
						break;

					case CAN_Q_CTEMPS_0:
					case CAN_Q_CTEMPS_1:
					case CAN_Q_CTEMPS_2:
					case CAN_Q_CTEMPS_3:
					case CAN_Q_CTEMPS_4:
					case CAN_Q_CTEMPS_5:
						answer_cell_temperatures(msg.data[1], &reply);
						while(CAN1_send(&reply)==-1);
						break;

					/*case CAN_Q_SELF_CHECK:
						break;*/

					case CAN_Q_DEMAND_RESET:
						msg.dlc = 4;
						msg.sid = CAN_ID;
						msg.data[0] = _time;
						while(CAN1_send(&msg)==-1);
						__delay_ms(50);	/* wait (at least) for message to be sent */
						RESET();

					default:
						return -1;
				}
				break;

			default:
				/* invalid size; report unknown */
				return -1;
		}

	}else if(msg.sid == CAN_ID_BMS_BROADCAST){

		/*
		 * Broadcasts
		 */

		switch(msg.dlc){

			case 6:
				switch(msg.data[1]){

					case CAN_B_BALANCING_TARGET:
						set_balancing_target(msg.data[2]);
						break;

					case CAN_B_SET_MODE:
						set_mode(msg.data[2]);
						break;

					default:
						return -1;
				}
				break;

			default:
				/* invalid size; report unknown */
				return -1;
		}

	}
	/* else: not for this module; ignore */

	return 0;
}


/**********************************************************************
 * Assign CAN1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv)) _C1Interrupt(void){

	CANdata msg, reply;

	if(C1INTFbits.RX0IF){
		msg.sid = C1RX0SIDbits.SID;
		msg.dlc = C1RX0DLCbits.DLC;
		msg.data[0] = C1RX0B1;
		msg.data[1] = C1RX0B2;
		msg.data[2] = C1RX0B3;
		msg.data[3] = C1RX0B4;
		C1RX0CONbits.RXFUL = 0;
		C1INTFbits.RX0IF = 0;

		if(CAN_handler(msg)<0){
			reply.sid = CAN_ID;
			reply.dlc = 4;
			reply.data[0] = _time;
			reply.data[1] = CAN_Q_UNKNOWN;
			while(CAN1_send(&reply)==-1);
		}
	}
	if(C1INTFbits.RX1IF){
		msg.sid = C1RX1SIDbits.SID;
		msg.dlc = C1RX1DLCbits.DLC;
		msg.data[0] = C1RX1B1;
		msg.data[1] = C1RX1B2;
		msg.data[2] = C1RX1B3;
		msg.data[3] = C1RX1B4;
		C1RX1CONbits.RXFUL = 0;
		C1INTFbits.RX1IF = 0;

		if(CAN_handler(msg)<0){
			reply.sid = CAN_ID;
			reply.dlc = 4;
			reply.data[0] = _time;
			reply.data[1] = CAN_Q_UNKNOWN;
			while(CAN1_send(&reply)==-1);
		}
	}

	IFS1bits.C1IF=0;
	return;
}


/**********************************************************************
 * Name:	emit_emergency
 * Args:	unsigned int code
 * Return:	-
 * Desc:	Sends respective CAN emergency message.
 **********************************************************************/
void emit_emergency(unsigned int code){

	CANdata AlertMsg;

	AlertMsg.sid = CAN_ID;
	AlertMsg.data[0] = _time;
	AlertMsg.data[1] = code;

	switch(code){

		/* critical and alert pairs have the same message apart from the code which is set above *
		 * So... no need to break, as it results in a smaller and simpler code                   */
		case CAN_E_V_CRITICAL_H:
		case CAN_E_V_ALERT_H:
			AlertMsg.dlc = 6;
			AlertMsg.data[2] = _highest_V;
			break;

		case CAN_E_V_CRITICAL_L:
		case CAN_E_V_ALERT_L:
			AlertMsg.dlc = 6;
			AlertMsg.data[2] = _lowest_V;
			break;

		case CAN_E_CELL_T_CRITICAL_H:
		case CAN_E_CELL_T_ALERT_H:
			AlertMsg.dlc = 8;
			AlertMsg.data[2] = _highest_cell_T;
			break;

		case CAN_E_CELL_T_CRITICAL_L:
		case CAN_E_CELL_T_ALERT_L:
			AlertMsg.dlc = 8;
			AlertMsg.data[2] = _lowest_cell_T;
			break;

		case CAN_E_SLAVE_T_CRITICAL_H:
		case CAN_E_SLAVE_T_ALERT_H:
			AlertMsg.dlc = 8;
			AlertMsg.data[2] = _highest_slave_T;
			AlertMsg.data[3] = _highest_load_T;
			break;

		case CAN_E_SLAVE_T_CRITICAL_L:
		case CAN_E_SLAVE_T_ALERT_L:
			AlertMsg.dlc = 6;
			AlertMsg.data[2] = _lowest_slave_T;
			break;

		case CAN_E_S_V_CONNECTIONS:
			AlertMsg.dlc = 6;
			AlertMsg.data[2] = _volt_fault_mask;
			break;

		case CAN_E_S_T_CONNECTIONS:
			AlertMsg.dlc = 6;
			AlertMsg.data[2] = _temp_fault_mask;
			break;

		case CAN_E_S_GENERIC_FAULT:
			AlertMsg.dlc = 4;
			break;

		/* Code does not exist or is CAN_E_UNKNOWN itself.. either way default to: */
		default:
			AlertMsg.dlc = 4;
			AlertMsg.data[0] = CAN_E_UNKNOWN;
			break;
	}

	while(CAN1_send(&AlertMsg)==-1);

	return;
}


/**********************************************************************
 * Name:	answer_SOC
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message to answer SOC query.
 **********************************************************************/
void answer_SOC(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 6;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_SOC;

	msg->data[2] = _SOC;

	return;
}


/**********************************************************************
 * Name:	answer_top_and_bottom_SOC
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message to answer SOC query.
 **********************************************************************/
void answer_top_and_bottom_SOC(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_TB_SOC;

	msg->data[2] = _highest_SOC;
	msg->data[3] = _lowest_SOC;

	return;
}


/**********************************************************************
 * Name:	answer_top_and_bottom_voltages
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the highest and lowest
 *          voltage in the stack.
 **********************************************************************/
void answer_top_and_bottom_voltages(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_TB_VOLT;

	msg->data[2] = _highest_V;
	msg->data[3] = _lowest_V;

	return;
}

/**********************************************************************
 * Name:	answer_average_voltage
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the average voltage.
 **********************************************************************/
void answer_average_voltage(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 6;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_AVG_VOLT;

	msg->data[2] = _average_V;

	return;
}


/**********************************************************************
 * Name:	answer_voltages
 * Args:	unsigned int code, CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with 3 of the 12 voltages
 *          according to selected code.
 *
 *          CAUTION! code validity is not checked!
 **********************************************************************/
void answer_voltages(unsigned int code, CANdata *msg){

	unsigned int i = (code - CAN_Q_VOLTAGES_0);	/* remove offset */

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = code;

	msg->data[2] = _voltages[i*2];
	msg->data[3] = _voltages[1+i*2];

	return;
}


/**********************************************************************
 * Name:	answer_cell_top_and_bottom_temperatures
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the most relevant
 *          temperatures of the cells in one stack.
 **********************************************************************/
void answer_cell_top_and_bottom_temperatures(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_TB_CTEMP;

	msg->data[2] = _highest_cell_T;
	msg->data[3] = _lowest_cell_T;

	return;
}


/**********************************************************************
 * Name:	answer_cell_average_temperatures
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the most relevant
 *          temperatures of the cells in one stack.
 **********************************************************************/
void answer_cell_average_temperatures(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 6;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_AVG_CTEMP;

	msg->data[2] = _average_cell_T;

	return;
}


/**********************************************************************
 * Name:	answer_cell_temperatures
 * Args:	unsigned int code, CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with 3 of the TEMP_SENSORS_N
 *          cell temperatures according to selected code.
 *
 *          Note:
 *          If installed sensors are less than 12, any index for the
 *          remaining cells will indicate 0ºC; it's up to the requester
 *          to know what he's doing.
 *          Returning an error is not a solution unless TEMP_SENSORS_N
 *          is always a multiple of 3!
 *          A better solution is to change the DLC, but then the
 *          message logic gets more complicated, this is a better
 *          compromise.
 *
 *          CAUTION! code validity is not checked!
 **********************************************************************/
void answer_cell_temperatures(unsigned int code, CANdata *msg){

	unsigned int i = (code - CAN_Q_CTEMPS_0);	/* remove offset */

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = code;

	if(i*2 < TEMP_SENSORS_N){
		msg->data[2] = _temperatures[i*2];
	}else{
		msg->data[2] = 0;
	}
	if(1+i*2 < TEMP_SENSORS_N){
		msg->data[3] = _temperatures[1+i*2];
	}else{
		msg->data[3] = 0;
	}

	return;
}


/**********************************************************************
 * Name:	answer_slave_temperatures
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the temperatures of the
 *          slave module.
 **********************************************************************/
void answer_slave_temperatures(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_STEMP;

	msg->data[2] = _highest_slave_T;
	msg->data[3] = _highest_load_T;

	return;
}


/**********************************************************************
 * Name:	answer_fault_masks
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the masks of bad connections
 *          of voltage and temperature sensing wires.
 **********************************************************************/
void answer_fault_masks(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 8;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_FMASKS;

	msg->data[2] = _volt_fault_mask;
	msg->data[3] = _temp_fault_mask;

	return;
}


/**********************************************************************
 * Name:	answer_balancing
 * Args:	CANdata *msg
 * Return:	-
 * Desc:	Constructs a CAN message with the balancing mask.
 **********************************************************************/
void answer_balancing(CANdata *msg){

	msg->sid = CAN_ID;
	msg->dlc = 6;

	msg->data[0] = _time;
	msg->data[1] = CAN_Q_BALANCING;

	msg->data[2] = _balance_mask;

	return;
}
