/**********************************************************************
 *   FST BMS --- slave
 *
 *   Main
 *      - device configuration
 *      - main process
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "Common/config.h"
#include "Common/CANcodes.h"
#include "config.h"

/* Only place where globals are not declared as 'extern' */
#define OWNER
#include "slave.h"
#undef OWNER

#include "timer.h"
#include "LED.h"
#include "CAN.h"
#include "LTC.h"
#include "temperatures.h"
#include "voltages.h"
#include "SOC.h"
#include "balancing.h"
#include "delay.h"


/*
 * Configuration bits
 */

/*
Clock switching and Fail-safe clock disabled
External clock multiplier = 4/2 = 2
*/
_FOSC(CSW_FSCM_OFF & HS2_PLL4);

/*
Watch-dog timer off --- enabling it in software instead after initializing period
Prescalers make time out ~3,072 seconds (RC oscillator! may oscillate with supply voltage)
*/
_FWDT(WDT_OFF & WDTPSB_3 & WDTPSA_512);

/*
Brown Out voltage = 2.7V & BOR circuit disabled
Power On Reset timer = 16ms
/MCLR enabled
*/
_FBORPOR(BORV_27 & PBOR_OFF & PWRT_16 & MCLR_EN);

/*
Code protection off
*/
_FGS(CODE_PROT_OFF);

/*
Select comm channel
*/
_FICD(PGD);


/**********************************************************************
 * Name:	set_CPU_priority
 * Args:    unsigned int n
 * Return:	previous priority OR error code
 * Desc:	Sets CPU priority to n, provided it is between 0 and 7.
 *          Provides a way to disable interruptions in critical parts
 *          of the code.
 *          Needs to be set to 6 or lower to allow interruptions,
 *          depending on interruptions' priorities.
 *          Previous priority returned for restoring purposes.
 **********************************************************************/
int set_CPU_priority(unsigned int n){

	unsigned int prevIPL;

	if(n<=7){
		prevIPL = SRbits.IPL;
		SRbits.IPL = n; 			/* Status Register . CPU priority 0-7 */
		return prevIPL;
	}
	return -1;
}


/**********************************************************************
 * Name:	set_mode
 * Args:    unsigned int mode
 * Return:	exit status
 * Desc:	Sets operating mode of the module.
 **********************************************************************/
int set_mode(unsigned int mode){

	switch(mode){

		case OP_INITIALIZING:	/* this mode has no effect implemented; master should ask for a RESET instead */
		case OP_NORMAL:
			_op_mode = mode;
			_balancing_target = V_CRITICAL_H;	/* so that when charging mode is set, no balancing is done until a new target is set */
			break;

		case OP_REGENERATION:
		case OP_CHARGING:
			_op_mode = mode;
			break;

		/* mode not recognized */
		default:
			return -1;
	}

	return 0;
}


/**********************************************************************
 * Name:	main_loop
 * Args:    -
 * Return:	-
 * Desc:	Main loop.
 **********************************************************************/
void main_loop(void){


	/* Assuming all is fine; gives a chance of reverting to normal after an alarm. *
	 * Should be the first thing in the loop: anything else should be able to set  *
	 * an alarm and increase the monitoring frequency                              */
	_monitor_period = MT_NORMAL;


	/*
	 * Get voltages and temperatures and test values
	 */

	monitor_voltages();
	monitor_temperatures();

	estimate_SOC();

	/*
	 * Set balancing loads' status
	 */

	/* set balancing and update global mask */
	_balance_mask = evaluate_balancing();
	update_discharge(_balance_mask);

	if(_balance_mask!=0){
		LED_on();
	}else{
		LED_off();
	}

	/* Clear watchdog timer so that device won't reset.     *
	 * If not run, device will reset in ~3sec (see config.) */
	ClrWdt();

	return;
}


int main(void){


	/*
	 * Globals init
	 */

	_time = 0;
	_monitor_period = MT_NORMAL;

	set_CPU_priority(7);
	set_mode(OP_INITIALIZING);
	update_loop_period(_monitor_period);

	{
		int i=0;
		for(i=0;i<16;i++){
			_temperatures[i] = 0;
		}
		for(i=0;i<12;i++){
			_voltages[i] = 0;
			_cell_SOC[i] = 0;
		}
	}

	_SOC = 0;
	_highest_SOC = 0;
	_lowest_SOC = 0;

	_highest_cell_T = 0;
	_lowest_cell_T = 0;
	_average_cell_T = 200;
	_highest_slave_T = 0;
	_lowest_slave_T = 0;
	_highest_load_T = 0;
	_total_V = 0;
	_average_V = 37000;
	_highest_V = 0;
	_lowest_V = 0;

	_balancing_target = V_CRITICAL_H;
	_balance_mask = 0;

	_temp_fault_mask = 0;
	_volt_fault_mask = 0;

	/*
	 * Configuration phase
	 */

	timer1_config();	/* for time keeping; no need for real time */

	LED_config();
	CAN1_config();

	/* Start ping */
	CANdata ping;
	ping.sid = CAN_ID;
	ping.dlc = 4;
	ping.data[0] = 0;	/* no value since it won't be synchronized yet */
	ping.data[1] = CAN_Q_START_PING;
	while(CAN1_send(&ping)==-1);

	temperatures_config();

	if(LTC_config()<0){		/* can't configure LTC */
		emit_emergency(CAN_E_S_GENERIC_FAULT);
		__delay_ms(50);	/* wait (at least) for message to be sent */
		/* Some evil and wicked spirit has possessed you! let the exorcism begin */
		RESET();
	}

	/* Enable Watchdog Timer; will remain active from here on. *
	 * From here, main loop (and the main loop only!) should   *
	 * reset the timer: we want to protect the main loop and   *
	 * the master knows if a slave stops responding, BUT it    *
	 * can't know if main loop is stuck and thus only old      *
	 * values are being reported back on a CAN interruption    */
	RCONbits.SWDTEN = 1;

	/* run once to initialize every variable *
	 * avoids reporting bad data to master   */
	main_loop();

	timer2_config();	/* for main loop */


	/*
	 * Initialization complete
	 */

	set_mode(OP_NORMAL);
	set_CPU_priority(0);

	while(1){
		Idle();
	}

	return 0;
}
