/**********************************************************************
 *   FST BMS --- slave
 *
 *   Balancing load control
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "Common/CANcodes.h"
#include "slave.h"
#include "LTC.h"
#include "CAN.h"
#include "delay.h"


/**********************************************************************
 * Name:	evaluate_balancing
 * Args:	-
 * Return:	calculated mask
 * Desc:	Evaluates which balancing loads should be active in order
 *          to balance the cells.
 **********************************************************************/
unsigned int evaluate_balancing(void){

	int i;
	unsigned int mask = 0b0000000000000000;

	if((_op_mode == OP_CHARGING) && (_highest_slave_T < TS_ALERT_H) && (_highest_load_T < TS_CRITICAL_H)){

		for(i=0;i<12;i++){
			                                                   /* faulty connections */
			if(((int)(_voltages[i] - _balancing_target) > BALANCE_HYSTERESIS_P) /*&& !((_volt_fault_mask & (1<<(i-1))) || (_volt_fault_mask & (1<<(i))))*/){
				mask |= (1<<i);
			}
		}

	}else{

		/* if there are critical conditions (over voltage), select those too regardless of mode or temperature! */
		for(i=0;i<12;i++){
			                                 /* faulty connections */
			if((_voltages[i] > V_CRITICAL_H) /*&& !((_volt_fault_mask & (1<<(i-1))) || (_volt_fault_mask & (1<<(i))))*/){
				mask |= (1<<i);
			}
		}
	}

	return mask;
}


/**********************************************************************
 * Name:	update_discharge
 * Args:	unsigned int mask
 * Return:	-
 * Desc:	Sets stack's discharge state according to mask.
 **********************************************************************/
void update_discharge(unsigned int mask){

	unsigned char ConfRegs[6]={0,0,0,0,0,0};

	/* These registers should not be changed; assuming they still have initial configuration */
	ConfRegs[0] = GFGR0;
	ConfRegs[3] = GFGR3;
	ConfRegs[4] = GFGR4;
	ConfRegs[5] = GFGR5;


	ConfRegs[1] = GFGR1 | (0b0000000011111111 & mask);
	ConfRegs[2] = GFGR2 | (0b00001111 & (mask >> 8));
	broadcast_write(WRCFG, 6, ConfRegs);

	if(broadcast_read(RDCFG, 6, ConfRegs) < 0){
		/* TODO: more descriptive message */
		emit_emergency(CAN_E_S_GENERIC_FAULT);
		__delay_ms(50);	/* wait (at least) for message to be sent */
		/* Some evil and wicked spirit has possessed you! let the exorcism begin */
		RESET();
	}

	if(	(ConfRegs[1] != (GFGR1 | (0b0000000011111111 & mask))) || (ConfRegs[2] != (GFGR2 | (0b00001111 & (mask >> 8)))) ){
		/* TODO: more descriptive message */
		emit_emergency(CAN_E_S_GENERIC_FAULT);
		__delay_ms(50);	/* wait (at least) for message to be sent */
		/* Some evil and wicked spirit has possessed you! let the exorcism begin */
		RESET();
	}

	return;
}


/**********************************************************************
 * Name:	set_balancing_target
 * Args:	unsigned int target
 * Return:	-
 * Desc:	Sets the target for balancing.
 **********************************************************************/
void set_balancing_target(unsigned int target){

	if((target < V_CRITICAL_H) && (target > V_ALERT_L)){
		_balancing_target = target;
	}

	return;
}
