/**********************************************************************
 *   FST BMS --- slave
 *
 *   SOC estimation
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>
#include <limits.h>

#include "slave.h"


/**********************************************************************
 * Name:	estimate_cell_SOC
 * Args:	unsigned int voltage
 * Return:	estimated SOC
 * Desc:	Provide an estimation of SOC in %*10 for a single cell.
 *
 *          Without Coulomb counting, there's no other way than
 *          moddeling the discharge curve!
 **********************************************************************/
unsigned int estimate_cell_SOC(unsigned int voltage/*, int current*/){

	unsigned int soc;

	if(voltage <= V_CRITICAL_L){
		soc = 0;
	}else if(voltage >= V_CRITICAL_H){
		soc = 1000;

	/* [V_CRITICAL_L, 3.4](V) --- [0, 20](%) */
	}else if(voltage <= 34000){
		soc = (unsigned int) ((float) voltage * 200/(34000 - V_CRITICAL_L) + (200 - 200*34000/(34000-V_CRITICAL_L)));

	/* ]3.4, 3.7](V) --- ]20, 90](%) */
	}else if(voltage <= 37000){
		soc = (unsigned int) ((float) voltage * (900-200)/(37000-34000) + (900 - (900-200)*37000/(37000-34000)));

	/* ]3.7, V_CRITICAL_H](V) --- ]90, 100](%) */
	}else{
		soc = (unsigned int) ((float) voltage * (1000-900)/(V_CRITICAL_H - 37000) + (1000 - (1000-900)*V_CRITICAL_H/(V_CRITICAL_H-37000)));
	}

	return soc;
}


/**********************************************************************
 * Name:	estimate_SOC
 * Args:	-
 * Return:	-
 * Desc:	Estimate the SOC.
 **********************************************************************/
void estimate_SOC(void){

	unsigned int prevIPL;
	int i;

	/* critical zone: don't want to report bad/intermediate values to master *
	 * cannot have a CAN interruption here                                   */
	prevIPL = set_CPU_priority(6);

	_SOC = 0;
	_highest_SOC = 0;
	_lowest_SOC = UINT_MAX;

	for(i=0;i<12;i++){
		_cell_SOC[i] = estimate_cell_SOC(_voltages[i]);

		if(_highest_SOC < _cell_SOC[i]){
			_highest_SOC = _cell_SOC[i];
		}

		if(_lowest_SOC > _cell_SOC[i]){
			_lowest_SOC = _cell_SOC[i];
		}

		_SOC += _cell_SOC[i];
	}

	_SOC = _SOC / 12;

	set_CPU_priority(prevIPL);

	return;
}
