/**********************************************************************
 *   FST BMS --- slave
 *
 *   Voltages
 *      - Interface for LTC IC
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>
#include <limits.h>
#include <stdlib.h>

#include "Common/config.h"
#include "Common/CANcodes.h"
#include "config.h"
#include "slave.h"
#include "timer.h"
#include "LTC.h"
#include "CAN.h"
#include "delay.h"


/**********************************************************************
 * Name:	open_wire_test
 * Args:	-
 * Return:	exit status
 * Desc:	Tests cell connections for wiring faults and sets mask of
 *          bad connections according to datasheet method, returning -1
 *          if a fault is found.
 *          In case of failed communication, -2 is returned and
 *          relevant values are updated with previous average.
 *
 *          A global mask of bad readings is set; note that it doesn't
 *          represent the bad wires, but the bad values: one bad
 *          connection affects two measurements.
 **********************************************************************/
int open_wire_test(void){

	unsigned int prevIPL;
	int error = 0;

	int i,j,k;

	unsigned int voltages[24];	/* voltages measurement 1 [12] | voltages measurement 2 [12] */

	/* holds the voltage registers as they are */
	unsigned char VoltRegs[18];


	/*
	 * Do the required measurements
	 */

	for(k=0;k<2;k++){

		/* measure with internal current sources on */
		broadcast_poll(STOWAD_ALL);
		/*__delay_ms(CONVERSION_TIME_MS);*/		/* Should be necessary in this case, but tests give the same result: *
		                                         * some bad readings that get away without triggering the check, and *
		                                         * then, detection.                                                  */

		if(broadcast_read(RDCV, 18, VoltRegs)!=0){
			return -2;
		}

		/* Mapping 18 8bit registers to 12 12bit integers         *
		 * easiest way: there are 2 voltages for each 3 registers */
		i = 0;
		j = 0;
		while(i<12){
			voltages[k*12+i] = (((VoltRegs[j+1] & 0b00001111) << 8) | VoltRegs[j]);
			/* do not use values in mV... waste of time */
			/*voltages[k*12+i] = ((voltages[k*12+i]) - 512) * 15;*/		/* conversion to mV*10 */

			voltages[k*12+i+1] = ((VoltRegs[j+1] & 0b11110000) >> 4) | (int)(VoltRegs[j+2] << 4);

			/* do not use values in mV... waste of time */
			/*voltages[k*12+i+1] = (voltages[k*12+i+1] - 512) * 15;*/	/* conversion to mV*10 */

			i+=2;
			j+=3;
		}
	}


	/*
	 * check for faults
	 */


	/* critical zone: don't want to report bad/intermediate values to master *
	 * cannot have a CAN interruption here                                   */
	prevIPL = set_CPU_priority(6);

	_volt_fault_mask = 0;

	/* V-/C0 is open (routed together) */   /* 0V */
	if((voltages[0] < 512) || (voltages[12] < 512)){
		_volt_fault_mask |= 1;	/*(1<<0);*/
		_voltages[0] = _average_V;
		error = -1;
	}
	/* C12 is open; C12 is routed with V+ in PCB... */
	if((voltages[11] < 512) || (voltages[23] < 512)){
		_volt_fault_mask |= (1<<12);
		_voltages[11] = _average_V;
		error = -1;
	}

	for(i=2;i<12;i++){
		                                   /*200mV*/                 /* 5375mV */
		if((abs(voltages[i+12] - voltages[i]) > 645) || (voltages[i+12] == 4095)){

			/* the bad connection is on (i-1) BUT i measurement is affected as well *
			 * the mask will thus represent the number of bad reads                 */
			_volt_fault_mask |= (1<<(i));
			/*_volt_fault_mask |= (1<<(i+1));*/		/* this measurement is also affected but it doesn't qualify as an open wire */

			/* to minimize influence on average determination and chances *
			 * of wrong triggering of emergencies, but still realistic    */
			_voltages[i-1] = _average_V;
			_voltages[i] = _average_V;
			error = -1;
		}
	}

	/*
	 * TODO ?
	 * datasheet suggests yet another test: measuring total voltage
	 * and comparing with sum of partials for top cell disconnect, but
	 * C12 is routed with V+ in the PCB, and there's a check for that already
	 */

	set_CPU_priority(prevIPL);

	return error;
}


/**********************************************************************
 * Name:	get_voltages
 * Args:	-
 * Return:	exit status
 * Desc:	Top level routine to acquire voltages.
 *          In case of failed read -1 is returned.
 *          -2 is returned if an open-wire test was run and failed.
 **********************************************************************/
int get_voltages(void){

	unsigned int prevIPL;
	int i,j;

	/* holds the voltage registers as they are */
	unsigned char VoltRegs[18];

	broadcast_poll(STCVAD_ALL);
	/*__delay_ms(CONVERSION_TIME_MS);*/		/* rather than waiting, we'll read values from previuos polling */

	if(broadcast_read(RDCV, 18, VoltRegs)!=0){
		return -1;
	}

	/* critical zone: don't want to report bad/intermediate values to master *
	 * cannot have a CAN interruption here                                   */
	prevIPL = set_CPU_priority(6);

	_volt_fault_mask = 0;

	/* Mapping 18 8bit registers to 12 12bit integers         *
	 * easiest way: there are 2 voltages for each 3 registers */
	i = 0;
	j = 0;
	while(i<12){
		_voltages[i] = (((VoltRegs[j+1] & 0b00001111) << 8) | VoltRegs[j]);
		_voltages[i] = ((_voltages[i]) - 512) * 15;		/* conversion to mV*10 */

		_voltages[i+1] = ((VoltRegs[j+1] & 0b11110000) >> 4) | (int)(VoltRegs[j+2] << 4);
		_voltages[i+1] = (_voltages[i+1] - 512) * 15;	/* conversion to mV*10 */
		i+=2;
		j+=3;
	}

	/* if bad connection is suspected, invoke a self-test and if it really is a bad connection *
	 * inform master and set value to something else than something with no meaning at all     */
	for(i=0;i<12;i++){
		/* suspect values starting from 0.5V from critical limits */
		if((_voltages[i] > V_CRITICAL_H+5000) || (_voltages[i] < V_CRITICAL_L-5000)){

			int open_wire_error = open_wire_test();
			if(open_wire_error == -1){
				/* open wire fault */
				return -2;
				/* only run open_wire_test() once */
				break;

			}else if(open_wire_error == -2){
				/* communication failed within open_wire_test() */
				return -1;
			}
		}
	}

	set_CPU_priority(prevIPL);

	return 0;
}


/**********************************************************************
 * Name:	monitor_voltages
 * Args:	-
 * Return:	-
 * Desc:	Top level routine to monitor voltages.
 **********************************************************************/
void monitor_voltages(void){

	unsigned int prevIPL;
	int i;
	int error;

	/* assuming all is fine */
	unsigned int period = MT_NORMAL;


	/*
	 * Acquire temperatures
	 */

	error = get_voltages();
	switch(error){

		case -1: /* LTC not responding; reset so it's reconfigured */
			/* TODO: more descriptive message */
			emit_emergency(CAN_E_S_GENERIC_FAULT);
			__delay_ms(50);	/* wait (at least) for message to be sent */
			/* Some evil and wicked spirit has possessed you! let the exorcism begin */
			RESET();
			break;	/* not that it matters */

		case -2:
			/* open wire fault */
			emit_emergency(CAN_E_S_V_CONNECTIONS);
			period = MT_CRITICAL;
			break;

		default:
			break;
	}

	/*
	 * Update globals
	 */

	/* critical zone: don't want to report bad/intermediate values to master *
	 * cannot have a CAN interruption here                                   */
	prevIPL = set_CPU_priority(6);

	_average_V = 0;
	_total_V   = 0;
	_highest_V = 0;			/* value that will be overridden for sure */
	_lowest_V  = UINT_MAX;	/* value that will be overridden for sure */

	for(i=0;i<12;i++){

		/* needs to be long to keep precision */
		_total_V += _voltages[i];

		if(_voltages[i] > _highest_V){
			_highest_V = _voltages[i];
		}
		if(_voltages[i] < _lowest_V){
			_lowest_V = _voltages[i];
		}
	}
	_average_V = _total_V / 12;

	set_CPU_priority(prevIPL);


	/*
	 * Test emergency conditions
	 */

	if(_highest_V >= V_CRITICAL_H){

		emit_emergency(CAN_E_V_CRITICAL_H);
		period = MT_CRITICAL;

	}else if(_highest_V >= V_ALERT_H){

		emit_emergency(CAN_E_V_ALERT_H);
		period = MT_ALERT;

	}

	if(_lowest_V <= V_CRITICAL_L){

		emit_emergency(CAN_E_V_CRITICAL_L);
		period = MT_CRITICAL;

	}else if(_lowest_V <= V_ALERT_L){

		emit_emergency(CAN_E_V_ALERT_L);
		period = MT_ALERT;

	}

	/* in case there was already another alert and monitoring period should remain faster */
	if(period <= _monitor_period){
		update_loop_period(period);
	}

	return;
}
