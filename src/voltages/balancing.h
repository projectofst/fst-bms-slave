/**********************************************************************
 *   FST BMS --- slave
 *
 *   Balancing load control header
 *      - prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __BALANCING_H__
#define __BALANCING_H__

unsigned int evaluate_balancing(void);
int update_discharge(unsigned int mask);

void set_balancing_target(unsigned int target);

#endif
