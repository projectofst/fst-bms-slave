/**********************************************************************
 *   FST BMS --- slave
 *
 *   PEC
 *      - PEC calculator (see LTC datasheet)
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "config.h"


/**********************************************************************
 * Name:	PEC_calculate
 * Args:    unsigned char data
 * Return:	PEC byte
 * Desc:	Calculates PEC for data.
 **********************************************************************/
unsigned char PEC_calculate(unsigned char *data, unsigned int n){

	unsigned int i, j;

	unsigned char PEC = 0b01000001;
	unsigned char DIN, IN0, IN1, IN2;

	unsigned char aux;

	for(j=0;j<n;j++){
		aux = 0b10000000;
		for(i=0;i<8;i++){		/* shift the whole byte */

			DIN = (data[j] & aux) >> (7-i);	/* isolates the ith MSB from byte on LSB position */

			IN0 = DIN ^ ((PEC & 0b10000000)>>7);	/* DIN XOR PEC[7] */
			IN1 = (PEC & 0b00000001) ^ IN0;			/* PEC[0] XOR IN0 */
			IN2 = ((PEC & 0b00000010)>>1) ^ IN0;	/* PEC[1] XOR IN0 */

			PEC = ((PEC << 1) & 0b11111000) | IN2<<2 | IN1<<1 | IN0;	/* shifts PEC 1 bit left and assigns 3 LSBs to IN2,IN1,IN0 */

			aux = aux >> 1;		/* shifts one bit from byte */
		}
	}
	return PEC;
}


/**********************************************************************
 * Name:	PEC_verify
 * Args:    unsigned char data, unsigned int n, unsigned char PEC
 * Return:	exit status
 * Desc:	Verifies if PEC is correct for given data.
 **********************************************************************/
int PEC_verify(unsigned char *data, unsigned int n, unsigned char PEC){

	if(PEC_calculate(data, n) == PEC){
		return 0;
	}
	return -1;
}
