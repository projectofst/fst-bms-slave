/**********************************************************************
 *   FST BMS --- slave
 *
 *   SPI
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __SPI_H__
#define __SPI_H__


void SPI2_config(void);

void SPI2_send(unsigned char *data, unsigned int n);
void SPI2_receive(unsigned int n, unsigned char *data);

void smite_me_if_I_know(void);

void SPI2_start(void);
void SPI2_end(void);

#endif
