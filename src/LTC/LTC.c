/**********************************************************************
 *   FST BMS --- slave
 *
 *   LTC
 *      - IC configuration
 *      - self-check
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "config.h"

/* Only place where vectors of addresses are not declared as 'extern' */
#define LTC
#include "LTC.h"
#undef LTC

#include "SPI.h"
#include "PEC.h"
#include "delay.h"


/**********************************************************************
 * Name:	LTC_config
 * Args:	-
 * Return:	exit status
 * Desc:	Configures all the modules required to communicate with
 *          the LTC and writes configuration register in LTC.
 **********************************************************************/
int LTC_config(void){

	int i;

	unsigned char ConfRegsWritten[6];
	unsigned char ConfRegsRead[6]={0,0,0,0,0,0};

	SPI2_config();

	/*
	 * Configuration register
	 */

	ConfRegsWritten[0] = GFGR0;
	ConfRegsWritten[1] = GFGR1;
	ConfRegsWritten[2] = GFGR2;
	ConfRegsWritten[3] = GFGR3;
	ConfRegsWritten[4] = GFGR4;
	ConfRegsWritten[5] = GFGR5;

	broadcast_write(WRCFG, 6, ConfRegsWritten);
	if(broadcast_read(RDCFG, 6, ConfRegsRead)){
		return -1;
	}

	for(i=0;i<6;i++){
		if(ConfRegsWritten[i] != ConfRegsRead[i]){
			return -1;
		}
	}

	/* provide first reading to avoid error on start-up */
	broadcast_poll(STCVAD_ALL);
	broadcast_poll(STTMPAD_INTERNAL);
	__delay_ms(CONVERSION_TIME_MS);

	return 0;
}


/**********************************************************************
 * Name:	broadcast_poll
 * Args:	unsigned int command
 * Return:	-
 * Desc:	Broadcast poll command.
 **********************************************************************/
void broadcast_poll(unsigned int command){

	unsigned char message[2];

	message[0] = command >> 8;
	message[1] = command;

	SPI2_start();
	SPI2_send(message, 2);
	SPI2_end();

	return;
}


/**********************************************************************
 * Name:	broadcast_write
 * Args:	unsigned int command, unsigned int n, unsigned char *data
 * Return:	-
 * Desc:	Broadcast write of n bytes.
 **********************************************************************/
void broadcast_write(unsigned int command, unsigned int n, unsigned char *data){

	unsigned int i;
	unsigned char message[n+3];

	message[0] = command >> 8;
	message[1] = command;

	for(i=0;i<n;i++){
		message[i+2] = data[i];
	}
	message[n+2] = (unsigned int) PEC_calculate(data, n);

	SPI2_start();
	SPI2_send(message, 3+n);
	SPI2_end();

	return;
}


/**********************************************************************
 * Name:	broadcast_read
 * Args:	unsigned int command, unsigned int n, unsigned char *data
 * Return:	exit status
 * Desc:	Broadcast read command of n length to data.
 **********************************************************************/
int broadcast_read(unsigned int command, unsigned int n, unsigned char *data){

	unsigned char command_message[2];
	unsigned char data_PEC;

	command_message[0] = command >> 8;
	command_message[1] = command;

	SPI2_start();
	SPI2_send(command_message, 2);

	/* vodoo? */
	smite_me_if_I_know();

	SPI2_receive(n, data);
	SPI2_receive(1, &data_PEC);
	SPI2_end();

	return PEC_verify(data, n, data_PEC);
}
