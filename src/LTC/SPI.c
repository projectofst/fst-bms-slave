/**********************************************************************
 *   FST BMS --- slave
 *
 *   SPI
 *      - module configuration
 *      - communication functions
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>


#define CS		LATGbits.LATG9

#define SPI_BAUDRATE FCY / 8	/* adjust according to pre scalar settings */
#define TCY_PER_BYTE 8*8+9		/* number of Tcy to wait for each byte; Tspi=8*Tcy times 1 byte = 8 bits * margin of 1/2 byte for each byte just in case */


/**********************************************************************
 * Name:	SPI2_config
 * Args:	-
 * Return:	-
 * Desc:	Configures and enables SPI channel 2 module
 **********************************************************************/
void SPI2_config(void){

	SPI2STATbits.SPISIDL = 0;		/* do not stop on idle (not needed I think, consider allowing i.e. setting it to 1) */
	SPI2STATbits.SPIROV = 0;		/* reset receive overflow flag bit; must be done by code or reception will be disabled! */

	SPI2CONbits.FRMEN = 0;			/* framed SPI disabled */
	SPI2CONbits.SPIFSD = 0;			/* framed SPI related */
	SPI2CONbits.DISSDO = 0;			/* SDO controlled by module */
	SPI2CONbits.MODE16 = 0;			/* 8 bit communication */
	SPI2CONbits.SMP = 0;			/* sample at middle of data input */
	SPI2CONbits.CKE = 0;			/* -CPHA; select H->L edge to change data */
	SPI2CONbits.SSEN = 0;			/* don't use /SS pin; driving it manually */
	SPI2CONbits.CKP = 1;			/* CPOL; negative clock polarity */
	SPI2CONbits.MSTEN = 1;			/* master mode */

	/* clock pre scalers; apply to FCY */
	/* for FOSC=30MHz, SCK = 937.5kHz < 1MHz */
	SPI2CONbits.SPRE = 0;			/* divide by 1 */
	SPI2CONbits.PPRE = 3;			/* divide by 16 */

	/* Interruptions */
	/*IFS1bits.SPI2IF = 0;*/		/* clear interruption flag */
	/*IPC6bits.SPI2IP = 6;*/		/* interruption priority */
	/*IEC1bits.SPI2IE = 1;*/		/* enable interruption */

	SPI2STATbits.SPIEN = 1;			/* module enable */

	TRISGbits.TRISG9 = 0;			/* CS */
	CS = 1;

	return;
}


/**********************************************************************
 * Name:	SPI2_start
 * Args:	-
 * Return:	-
 * Desc:	Used to signal the beginning of a message.
 **********************************************************************/
void SPI2_start(void){
	CS = 0;
	return;
}


/**********************************************************************
 * Name:	SPI2_end
 * Args:	-
 * Return:	-
 * Desc:	Used to end a message, enabling the SPI interruption to
 *          unset the CS and disable itself afterwards.
 **********************************************************************/
void SPI2_end(void){
	while(!SPI2STATbits.SPIRBF);	/* wait while reception buffer is empty */
	CS = 1;
	return;
}


/**********************************************************************
 * Name:	SPI2_send
 * Args:	unsigned char *data
 * Return:	-
 * Desc:	Sends a string of n bytes through SPI channel 2.
 **********************************************************************/
void SPI2_send(unsigned char *data, unsigned int n){

	unsigned int i=0;
	char garbage;

	while(SPI2STATbits.SPITBF);		/* wait while transmission buffer is full */

	while(i<n){
		SPI2BUF = data[i];
		while(SPI2STATbits.SPITBF);	/* wait while transmission buffer is full */
		garbage = SPI2BUF;
		i++;
	}

	return;
}


/**********************************************************************
 * Name:	SPI2_receive
 * Args:	unsigned int n, unsigned char *data
 * Return:	-
 * Desc:	Reads a string from SPI2 on request.
 **********************************************************************/
void SPI2_receive(unsigned int n, unsigned char *data){

	unsigned int i=0;

	while(i < n){						/* still expecting more bytes */
		while(SPI2STATbits.SPITBF);		/* wait while transmission buffer is full */
		SPI2BUF = 255;					/* send 11111111 continuously */
		while(!SPI2STATbits.SPIRBF);	/* wait while reception buffer is empty */
		data[i] = SPI2BUF;
		i++;
	}

	return;
}


/**********************************************************************
 * Name:	smite_me_if_I_know
 * Args:	-
 * Return:	-
 * Desc:	Name says all.
 *          Seriously now, receive() gets one byte behind somehow,
 *          this discards that byte...
 *          I would understand, but sending this extra 255 changes
 *          nothing in the SPI line (NOTHING), yet it's undoubtedly
 *          one 255 too many...
 **********************************************************************/
void smite_me_if_I_know(void){

	char garbage;

	while(SPI2STATbits.SPITBF);
	SPI2BUF = 255;
	while(!SPI2STATbits.SPIRBF);
	garbage = SPI2BUF;		/* discard byte */

	return;
}
