/**********************************************************************
 *   FST BMS --- slave
 *
 *   LTC
 *      - function prototypes
 *      - definitions related to LTC IC
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __LTC_H__
#define __LTC_H__

#include "Common/config.h"

/*
 * Prototypes
 */

int LTC_config(void);

void broadcast_poll(unsigned int command);
void broadcast_write(unsigned int command, unsigned int n, unsigned char *data);
int broadcast_read(unsigned int command, unsigned int n, unsigned char *data);


/*
 * Parameters
 */

#define LONGEST_MESSAGE 20
#define CONVERSION_TIME_MS 15	/* time between poll command and conversion completed in ms (depends on CDC) */


/*
 * Configuration registers default values
 */

#define CDC 1	/* 0-7; comparator duty cycle */

#define GFGR0 CDC	/* all zero/default except for CDC bits which are the LSBs */

/* disable balancing for all cells + disable interrupts for all cells and clear flags */
#define GFGR1 0b00000000
#define GFGR2 0b11110000
#define GFGR3 0b11111111

#define GFGR4 (V_CRITICAL_L / 15 / 16 + 31)
#define GFGR5 (V_CRITICAL_H / 15 / 16 + 32)


/*
 * Commands
 */


/* write configuration register group */
#define WRCFG 0x01C7
/* read configuration register group */
#define RDCFG 0x02CE

/* read all cell voltage group */
#define RDCV 0x04DC
/* read cell voltages 1-4 */
#define RDCVA 0x06D2
/* read cell voltages 5-8 */
#define RDCVB 0x08F8
/* read cell voltages 9-12 */
#define RDCVC 0x0AF6

/* read flag register group */
#define RDFLG 0x0CE4

/* read temperature register group */
#define RDTMP 0x0EEA

/* start cell voltage ADC conversions and poll status */
#define STCVAD_ALL			0x10B0

#ifdef LTC
	const unsigned int STCVAD_CELL[12] = {0x11B7,0x12BE,0x13B9,0x14AC,0x15AB,0x16A2,0x17A5,0x1888,0x198F,0x1A86,0x1B81,0x1C94};
#else
	extern const unsigned int STCVAD_CELL[12];
#endif

#define STCVAD_CLEAR_FF		0x1D93
#define STCVAD_SELF_TEST_1	0x1E9A
#define STCVAD_SELF_TEST_2	0x1F9D

/* start open-wire ADC conversions and poll status */
#define STOWAD_ALL			0x2020

#ifdef LTC
	const unsigned int STOWAD_CELL[12] = {0x2127,0x222E,0x2329,0x243C,0x253B,0x2632,0x2735,0x2818,0x291F,0x2A16,0x2B11,0x2C04};
#else
	extern const unsigned int STOWAD_CELL[12];
#endif

/* start temperature ADC conversions and poll status */
#define STTMPAD_ALL			0x3050
#define STTMPAD_EXTERNAL_1	0x3157
#define STTMPAD_EXTERNAL_2	0x325E
#define STTMPAD_INTERNAL	0x3359
#define STTMPAD_SELF_TEST_1	0x3E7A
#define STTMPAD_SELF_TEST_2	0x3F7D

/* poll ADC converter status */
#define PLADC 0x4007

/* poll insterrupt status */
#define PLINT 0x5077

/* start diagnose and poll status */
#define DAGN 0x5279

/* read diagnose register */
#define RDDGNR 0x546B

/* start cell voltage ADC conversions and poll status with discharge permitted */
#define STCVDC_ALL			0x60E7
#ifdef LTC
	const unsigned int STCVDC_CELL[12] = {0x61E0,0x62E9,0x63EE,0x64FB,0x65FC,0x66F5,0x67F2,0x68DF,0x69D8,0x6AD1,0x6BD6,0x6CC3};
#else
	extern const unsigned int STCVDC_CELL[12];
#endif


/* start open-wire ADC conversions and poll status with discharge permitted */
#define STOWDC_ALL			0x7097
#ifdef LTC
	const unsigned int STOWDC_CELL[12] = {0x7190,0x7299,0x739E,0x748B,0x758C,0x7685,0x7782,0x78AF,0x79A8,0x7AA1,0x7BA6,0x7CB3};
#else
	extern const unsigned int STOWDC_CELL[12];
#endif


#endif
