/**********************************************************************
 *   FST BMS --- slave
 *
 *   ADC
 *      - module configuration
 *      - acquisition functions
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>

#include "delay.h"

/* Fosc = 30 MHz; Fcy = Fosc/4 = 7.5 MHz; Tcy = 1/Fcy = 133 ns;
   Tconv >= 10 us    (specification)  : conversion time
   Cconv >= 14       (specification)  : conversion cycles
   Tad >= 333.33 ns  (specification)  : period of ADC clock

   ADCS = ceil[ ((2*Tconv)/(Cconv*Tcy))-1 ] = 10
   Tad = Tcy*(ADCS+1)/2 = 731.5 ns
   Tconv = 14*Tad = 10.241 us                                    */
#define ADC_CONV_CLOCK 10



/**********************************************************************
 * Name:	ADC_config
 * Args:	-
 * Return:	-
 * Desc:	Configures and enables ADC module
 **********************************************************************/
void ADC_config(void){

	ADCON1bits.ADSIDL = 1;				/* stop on idle                         */
	ADCON1bits.FORM   = 0;				/* output format: unsigned int          */
	ADCON1bits.SSRC   = 7;				/* auto-start conversion                */
	ADCON1bits.ASAM   = 0;				/* manual start sample                  */
	ADCON1bits.SAMP   = 0;				/* sample enable(1) (ASAM=0) and        *
										 * conversion start(0) (SSRC=0)         */
	/*ADCON1bits.DONE = 0;*/			/* conversion is done (cleared on new   *
										 * conversion or by user)               */

	ADCON2bits.VCFG   = 3;				/* external voltage reference           */
	ADCON2bits.CSCNA  = 0;				/* scan disable                         */
	/*ADCON2bits.BUFS = 0;*/			/* select buffer; only valid for BUFM=1 */
	ADCON2bits.SMPI   = 0;				/* one sample per interrupt             */
	ADCON2bits.BUFM   = 0;				/* one 16bit buffer                     */
	ADCON2bits.ALTS	  = 0;				/* always use MuxA                      */

	ADCON3bits.SAMC   = 1;				/* auto sample time: 1 Tad              */
	ADCON3bits.ADRC   = 0;				/* use system clock conversion          */
	ADCON3bits.ADCS   = ADC_CONV_CLOCK;	/* conversion clock                     */

	ADCHSbits.CH0NA   = 0;				/* ch0 negative input for MuxA as Vref- */
	ADCHSbits.CH0SA   = 3;				/* ch0 positive input for MuxA as AN3   */
	ADCHSbits.CH0NB   = 0;				/* ch0 negative input for MuxB as Vref- */
	ADCHSbits.CH0SB   = 3;				/* ch0 positive input for MuxB as AN3   */

	ADCSSLbits.CSSL2  = 0;				/* no port should be scanned            */

	TRISBbits.TRISB3  = 1;				/* set AN3 as input                     */
	ADPCFGbits.PCFG3  = 0;				/* set AN3 as analogue port             */

	ADCON1bits.ADON   = 1;				/* ADC module enable                    */

	return;
}


/**********************************************************************
 * Name:	sample_AN3
 * Args:	-
 * Return:	unsigned int ADCBUF0
 * Desc:	Samples AN3 and returns the value 0-4095 which maps 0-5(V)
 **********************************************************************/
unsigned int sample_AN3(void){

	ADCON1bits.SAMP = 1;			/* starts sampling; resets ADCON1bits.DONE automatically */
	while(!ADCON1bits.DONE);	/* waits for sampling and conversion */

	/* Not needed with -O0, but needed with -O1 *
	 * don't know why though.                   *
	 * Without it, value is ~2^12-1, which      *
	 * results in detected wire fault           */
	/*__delay_us(1);*/
	/*	Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();
		Nop();*/


	return ADCBUF0;
}
