/**********************************************************************
 *   FST BMS --- slave
 *
 *   Timer
 *      - module configuration
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <p30F6012A.h>
#include <math.h>
#include <limits.h>

#include "Common/CANcodes.h"
#include "config.h"
#include "slave.h"
#include "timer.h"
#include "ADC.h"
#include "LTC.h"
#include "CAN.h"
#include "temperatures.h"
#include "delay.h"


/*
 * NTC measurement related values
 */

/* NTC related values */
#define R_25 4700
#define A_1 0.003354016
#define B_1 0.000256985
#define C_1 0.000002620131
#define D_1 0.0000000638309

/* conversion factor from Kelvin to Celsius */
#define K_TO_C (-273)

/* addresses for special function NTCs */
#define MPU_TEMP_ADD 14
#define BALANCE_R_TEMP_TOP 13
#define BALANCE_R_TEMP_BOTTOM 15


/**********************************************************************
 * Name:	temperatures_config
 * Args:	-
 * Return:	-
 * Desc:	Configures the ADC and digital ports to operate the MUXs
 *          to provide temperature readings.
 **********************************************************************/
void temperatures_config(void){

	ADC_config();

	/* MUX enable ports (active low!); set as output and initialized high */
	TRISDbits.TRISD1 = 0;
	TRISDbits.TRISD2 = 0;
	LATDbits.LATD1 = 1;
	LATDbits.LATD2 = 1;

	/* MUX address ports; set as output */
	TRISDbits.TRISD3 = 0;
	TRISDbits.TRISD4 = 0;
	TRISDbits.TRISD5 = 0;

	return;
}


/**********************************************************************
 * Name:	get_temp
 * Args:	unsigned int n
 * Return:	temperature value in ºC*10
 * Desc:	Probes temperature from address n.
 *          Output is written to a global vector.
 *          If there is a faulty connection, it will be detected and
 *          the return value set to INT_MAX.
 **********************************************************************/
int get_temp(unsigned int n){

	unsigned int ADCvalue;
	unsigned int MUXaddress = n;

	float R = 0;
	float lnRR25 = 0;		/* auxiliary; saves some calculations */
	float ADCmaxint = 0; 	/* auxiliary; saves some calculations */


	if(n<8){	/* enable first MUX */
		LATDbits.LATD1 = 0;
	}else if(n<16){	/* enable second MUX */
		LATDbits.LATD2 = 0;
		MUXaddress = n-8;
	}else{
		return -1000;	/* this value constitutes an error for sure as it would mean (-100ºC) */
	}

	/* set address */
	LATDbits.LATD3 = 1 & MUXaddress;
	LATDbits.LATD4 = (2 & MUXaddress) >> 1;
	LATDbits.LATD5 = (4 & MUXaddress) >> 2;

	/*
	 * Note: enable and switching time can take up to ~20ns which is faster than the clock frequency.
	 * Still, consider adding a delay here if measurements are not consistent.
	 */

	ADCvalue = sample_AN3();
	/* disable muxes to save energy */
	LATDbits.LATD1 = 1;
	LATDbits.LATD2 = 1;

	/* means broken connection; see get_cell_temperatures() *
	 * '-100' factor may need to be adjusted                */
	if(ADCvalue > (4096 - 100)){
		return INT_MAX;
	}

	/*
	 * Note: Acquisition and calculations may take too long to be worthwhile.
	 * Measure/compare gains and use table if needed.
	 */

	ADCmaxint = (float) ADCvalue/4096;
	R = 1000*ADCmaxint/(1-ADCmaxint);
	lnRR25 = log(R/R_25);
	return (int)( (1/(A_1 + B_1*lnRR25 + C_1*pow(lnRR25,2) + D_1*pow(lnRR25,3)) + K_TO_C) * 10);
}


/**********************************************************************
 * Name:	get_LTC_temperature
 * Args:	-
 * Return:	exit status
 * Desc:	Reads LTC's internal temperature and stores it in ºC*10.
 *          In case of bad read, value is left unchanged and -1 is
 *          returned. -2 is returned for a detected thermal shutdown.
 **********************************************************************/
int get_LTC_temperature(void){

	unsigned int temp;

	/* holds the voltage registers as they are */
	unsigned char TempRegs[5] = {0,0,0,0,0};

	broadcast_poll(STTMPAD_INTERNAL);
	/*__delay_ms(CONVERSION_TIME_MS);*/		/* rather than waiting, we'll read values from previuos polling */

	if(broadcast_read(RDTMP, 5, TempRegs)!=0){
		return -1;
	}

	/* get internal temperature reading */
	temp = TempRegs[3] | ((unsigned int) (TempRegs[4] & 0b00001111) << 8);


	/* map value to ºC*10 */
	_temperatures[LTC_TEMP_INDX] = (int)((long)(temp - 512)*150/80 + K_TO_C*10);

	/* thermal shutdown occurred */
	if(TempRegs[4] & 0b0000000000010000){
		return -2;
	}

	return 0;
}


/**********************************************************************
 * Name:	get_NTC_temperatures
 * Args:	-
 * Return:	exit status
 * Desc:	Top level routine to acquire all NTC's temperatures:
 *          cells, MPU and balancing load.
 **********************************************************************/
int get_NTC_temperatures(void){

	unsigned int prevIPL;
	int i;
	int error=0;

	/*
	 * For NTCs:
	 * +5V --^^^--o (to NTC) o--|>
	 *            |
	 *            ADC
	 * if there is a bad connection, value read will be ~2^12-1 (+5V on the ADV)
	 * this doesn't cover a damaged PCB! ..nothing does by the way
	 */


	/* critical zone: don't want to report bad/intermediate values to master *
	 * cannot have a CAN interruption here                                   */
	prevIPL = set_CPU_priority(6);

	_temp_fault_mask = 0;


	/*
	 * Cells' temperatures
	 */

	for(i=0;i<TEMP_SENSORS_N;i++){
		/* Assuming that sensors are on the highest index positions. *
		 * Otherwise do get_temp(i).                                 */
		_temperatures[i] = get_temp(i+12-TEMP_SENSORS_N);
	}

	/* detect bad connections; set value to something else than something with no meaning at all */
	for(i=0;i<TEMP_SENSORS_N;i++){
		if(_temperatures[i] == INT_MAX){
			/* to minimize influence on average determination and chances *
			 * of wrong triggering of emergencies, but still realistic    */
			_temperatures[i] = _average_cell_T;

			_temp_fault_mask |= (1<<i);
			error--;
		}
	}

	/*
	 * MPU's and Balancing load's temperatures
	 */

	_temperatures[MPU_TEMP_INDX] = get_temp(MPU_TEMP_ADD);

	/* nearest the top of the stack */
	_temperatures[BALLANCE_TEMP_INDX_TOP] = get_temp(BALANCE_R_TEMP_TOP);
	/* nearest the bottom of the stack */
	_temperatures[BALLANCE_TEMP_INDX_BOTTOM] = get_temp(BALANCE_R_TEMP_BOTTOM);

	/* detect bad connections; set value to something else than something with no meaning at all */
	for(i=TEMP_SENSORS_N;i<TEMP_SENSORS_N+4;i++){
		if((_temperatures[i] == INT_MAX) && (i != LTC_TEMP_INDX)){

			/* won't trigger an alert on top of the bad connection fault; won't interfere with the other values triggering an emergency */
			_temperatures[i] = 250;	/* 25ºC */

			_temp_fault_mask |= (1<<i);
			error--;
		}
	}

	set_CPU_priority(prevIPL);

	return error;
}


/**********************************************************************
 * Name:	monitor_temperatures
 * Args:	-
 * Return:	-
 * Desc:	Top level routine to monitor stack's temperatures.
 **********************************************************************/
void monitor_temperatures(void){

	unsigned int prevIPL;
	int error;
	int i;

	int tempC_CH;
	int tempC_AH;
	int tempC_AL;
	int tempC_CL;


	/* assuming all is fine */
	unsigned int period = MT_NORMAL;


	/*
	 * Acquire temperatures
	 */

	if(get_NTC_temperatures()!=0){
		/* open wire fault */
		emit_emergency(CAN_E_S_T_CONNECTIONS);
		period = MT_ALERT;
	}

	error = get_LTC_temperature();
	switch(error){

		case -1: /* LTC not responding; reset so it's reconfigured */
			/* TODO: more descriptive message */
			emit_emergency(CAN_E_S_GENERIC_FAULT);
			__delay_ms(50);	/* wait (at least) for message to be sent */
			/* Some evil and wicked spirit has possessed you! let the exorcism begin */
			RESET();
			break;	/* not that it matters */

		case -2: /* LTC had a thermal shutdown; reset so it's reconfigured */
			/* TODO: more descriptive message */
			emit_emergency(CAN_E_S_GENERIC_FAULT);
			__delay_ms(50);	/* wait (at least) for message to be sent */
			/* Some evil and wicked spirit has possessed you! let the exorcism begin */
			RESET();
			break;	/* not that it matters */

		default:
			break;
	}


	/*
	 * Update globals
	 *
	 * Note: if value measured doesn't make sense it should be
	 * discarded and set to an intermediate level:
	 * better use this false value than having one value mess this up.
	 * This should be done elsewhere!
	 */

	/* critical zone: don't want to report bad/intermediate values to master *
	 * cannot have a CAN interruption here                                   */
	prevIPL = set_CPU_priority(6);

	_average_cell_T = 0;
	_highest_cell_T = INT_MIN;	/* value that will be overridden for sure */
	_lowest_cell_T  = INT_MAX;	/* value that will be overridden for sure */

	for(i=0;i<TEMP_SENSORS_N;i++){

		/* intermediate step; for signed int, maximum temperature per cell before overflow is ~273.0ºC */
		_average_cell_T += _temperatures[i];

		if(_temperatures[i] > _highest_cell_T){
			_highest_cell_T = _temperatures[i];
		}
		if(_temperatures[i] < _lowest_cell_T){
			_lowest_cell_T = _temperatures[i];
		}
	}
	_average_cell_T = _average_cell_T / TEMP_SENSORS_N;

	if(_temperatures[MPU_TEMP_INDX] <= _temperatures[LTC_TEMP_INDX]){
		_highest_slave_T = _temperatures[LTC_TEMP_INDX];
		_lowest_slave_T  = _temperatures[MPU_TEMP_INDX];
	}else{
		_highest_slave_T = _temperatures[MPU_TEMP_INDX];
		_lowest_slave_T  = _temperatures[LTC_TEMP_INDX];
	}

	if(_temperatures[BALLANCE_TEMP_INDX_TOP] <= _temperatures[BALLANCE_TEMP_INDX_BOTTOM]){
		_highest_load_T = _temperatures[BALLANCE_TEMP_INDX_BOTTOM];
	}else{
		_highest_load_T = _temperatures[BALLANCE_TEMP_INDX_TOP];
	}

	set_CPU_priority(prevIPL);


	/*
	 * Test emergency conditions
	 */

	/* use appropriate limits for cells*/
	if(_op_mode == OP_CHARGING){
		/* charging */
		tempC_CH = TCC_CRITICAL_H;
		tempC_AH = TCC_ALERT_H;
		tempC_AL = TCC_ALERT_L;
		tempC_CL = TCC_CRITICAL_L;
	}else{
		/* discharging */
		tempC_CH = TCD_CRITICAL_H;
		tempC_AH = TCD_ALERT_H;
		tempC_AL = TCD_ALERT_L;
		tempC_CL = TCD_CRITICAL_L;
	}

	/* test cell temperatures */
	if(_highest_cell_T >= tempC_CH){

		emit_emergency(CAN_E_CELL_T_CRITICAL_H);
		period = MT_CRITICAL;

	}else if(_highest_cell_T >= tempC_AH){

		emit_emergency(CAN_E_CELL_T_ALERT_H);
		period = MT_ALERT;

	}
	if(_lowest_cell_T <= tempC_CL){

		emit_emergency(CAN_E_CELL_T_CRITICAL_L);
		period = MT_CRITICAL;

	}else if(_lowest_cell_T <= tempC_AL){

		emit_emergency(CAN_E_CELL_T_ALERT_L);
		period = MT_ALERT;

	}

	/* test slave temperatures                    *
	 * balancing load doesn't trigger emergencies */
	if(_highest_slave_T >= TS_CRITICAL_H){

		emit_emergency(CAN_E_SLAVE_T_CRITICAL_H);
		period = MT_CRITICAL;

	}else if(_highest_slave_T >= TS_CRITICAL_H){

		emit_emergency(CAN_E_SLAVE_T_ALERT_H);
		period = MT_ALERT;

	}
	if(_lowest_slave_T <= TS_CRITICAL_L){

		emit_emergency(CAN_E_SLAVE_T_CRITICAL_L);
		period = MT_CRITICAL;

	}else if(_lowest_slave_T <= TS_ALERT_L){

		emit_emergency(CAN_E_SLAVE_T_ALERT_L);
		period = MT_ALERT;

	}

	/* in case there was already another alert and monitoring period should remain faster */
	if(period <= _monitor_period){
		update_loop_period(period);
	}

	return;
}
