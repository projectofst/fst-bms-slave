/**********************************************************************
 *   FST BMS --- slave
 *
 *   Temperatures
 *      - parameters
 *      - functions prototypes
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#ifndef __TEMPERATURES_H__
#define __TEMPERATURES_H__

#include "Common/config.h"
#include "CAN.h"


/*
 * Parameters
 */

/* indexes for slave temperatures */
#define MPU_TEMP_INDX TEMP_SENSORS_N
#define LTC_TEMP_INDX (TEMP_SENSORS_N+1)
#define BALLANCE_TEMP_INDX_TOP (TEMP_SENSORS_N+2)
#define BALLANCE_TEMP_INDX_BOTTOM (TEMP_SENSORS_N+3)


/*
 * Prototypes
 */

void temperatures_config(void);
void monitor_temperatures(void);

#endif
